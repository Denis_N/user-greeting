package com.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Denis Nikashin.
 * @version 1.0-SNAPSHOT
 */
@WebServlet("/")
public class Welcome extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                                                    throws ServletException, IOException {

        String username = req.getParameter("username");
        req.setAttribute("name", username);
        req.getRequestDispatcher("page.jsp").forward(req, resp);
    }
}
